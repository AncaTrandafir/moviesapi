﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Movies.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Movies;
using MoviesAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using MoviesAPI.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MoviesAPI.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace MoviesAPI.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class MoviesController : ControllerBase
    {

        private IMovieService movieService;
        private IUserService userService;


        public MoviesController(IMovieService movieService, IUserService userService)
        {
            this.movieService = movieService;
            this.userService = userService;
        }


        /// <summary>
        /// Retrieves a list of all movies from DB.
        /// </summary>
        /// <returns>List of movies</returns>
        // GET: movies
        //[AllowAnonymous]
        //[HttpGet]
        //public IActionResult GetMovies([FromQuery] PagingParameters pagingParameters)
        //{
        //    var movies = movieService.GetMoviesPaginated(pagingParameters);

        //    var metadata = new
        //    {
        //        movies.TotalCount,
        //        movies.PageSize,
        //        movies.CurrentPage,
        //        movies.TotalPages,
        //        movies.HasNext,
        //        movies.HasPrevious
        //    };

        //    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

        //    return Ok(movies);
        //}




        /// <summary>
        /// Retrieves a list of all movies from DB.
        /// </summary>
        /// <returns>List of movies</returns>
        // GET: movies
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetMovies()
        {
            var movies = movieService.GetMovies();

            return Ok(movies);
        }






        /// <summary>
        /// Retrieves a specific movie by id, list of its comments included.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the movie specified by id and its list of comments.</returns>
        // GET: movies/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public Movie GetMovieById(long id)
        {
           return movieService.GetMovieById(id);      
        }





        /// <summary>
        /// Retrieves filtered movies, added between certain dates, also alphabetically ordered.
        /// </summary>
        /// <remarks>
        /// Sample URL request:
        ///    https://localhost:44335/movies/filter?$from=2020-05-15T00:00:00&to=2020-05-17T00:00:00
        /// Sample parameter: yyyy-MM-dd   
        /// </remarks>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>A list of movies with dateAdded between the two specified dates.</returns>
        // GET: movies/filter?from=a&to=b
        [AllowAnonymous]
        [HttpGet("filter")]
        public IEnumerable<MovieGetModel> GetFilteredMovies(
            [FromQuery] string from,
            [FromQuery] string to)
        {
            return movieService.FilterByDate(from, to);
        }








        /// <summary>
        /// Edit any properties of a specific movie you mention by id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        // PUT: movie/5
        [HttpPut("{id}")]
        public IActionResult PutMovie(long id, [FromBody] Movie movie)
        {
            var result = movieService.Update(id, movie);
            return Ok(result);
        }








        /// <summary>
        /// Creates a new movie.
        /// </summary>
        /// <param name="movie"></param>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Movies
        ///      {
        ///         "dateAdded": "2019-04-05",
        ///         "description": "Frodo",
        ///         "director": "Steven Spielberg",
        ///         "duration": "120",
        ///         "rating": "10",
        ///         "title": "Stapanul inelelor3",
        ///         "watched": "True",
        ///         "yearOfRelease": "2000"
        ///       }
        ///
        /// </remarks>
        /// <param name="movie"></param>
        /// <returns>A newly created movie</returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response> 
        // POST: /movies
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public void PostMovie([FromBody] MoviePostModel movie)
        {
            User user = userService.GetCurrentUser(HttpContext);
            movieService.Create(movie, user);
        }








        /// <summary>
        /// Deletes the movie tou specify by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the movie deleted.</returns>
        // DELETE: /5







        private bool MovieExists(long id)
        {
            return movieService.GetMovies().Any(e => e.Id == id);
        }











    }
}

