﻿using MoviesAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Movies.Models;
using MoviesAPI.Models;

namespace MoviesAPI.Services
{
    public interface IMovieService
    {
        PaginatedList<MovieGetModel> GetMoviesPaginated(PagingParameters pagingParameters);
        IEnumerable<MovieGetModel> GetMovies();
        IEnumerable<MovieGetModel> FilterByDate(string from, string to);
        Movie GetMovieById(long id);
        Movie Create(MoviePostModel movie, User user);
        Movie Update(long id, Movie movie);
        Movie Delete(long id);
    }


    public class MovieService : IMovieService
    {
        private MoviesDbContext context;
        public MovieService(MoviesDbContext context)
        {
            this.context = context;
        }



        //public Movie Create(Movie movieToAdd)
        //{
        //    // TODO: how to store the user that added the movie as a field in Movie?
        //    context.Movies.Add(movieToAdd);
        //    context.SaveChanges();
        //    return movieToAdd;
        //}





        public Movie Create(MoviePostModel movie, User user)
        {
            // store the user that added the movie

            Movie movieToAdd = MoviePostModel.PostMovieModel(movie);
            movieToAdd.AddedBy = user;

            context.Movies.Add(movieToAdd);
            context.SaveChanges();
            return movieToAdd;
        }






        public Movie Delete(long id)
        {
            var existing = context.Movies
                .Include(m => m.Comments)
                .FirstOrDefault(movie => movie.Id == id);
            if (existing == null)
            {
                return null;
            }
            context.Remove(existing);
            context.SaveChanges();

            return existing;
        }








        public PaginatedList <MovieGetModel> GetMoviesPaginated(PagingParameters pagingParameters)
        {
            var result = context.Movies
                .OrderBy(m => m.Id);

            return PaginatedList<MovieGetModel>.ToPagedList(result.Select(m => MovieGetModel.GetMovieModel(m)), pagingParameters.PageNumber, pagingParameters.PageSize);

            
        }











        // Nepaginat
        public IEnumerable<MovieGetModel> GetMovies()     // get de view model care are nr de comm
        {
            IQueryable<Movie> result = context.Movies
                                        .Include(m => m.Comments);

            return result.Select(m => MovieGetModel.GetMovieModel(m));
        }









        public IEnumerable<MovieGetModel> FilterByDate(string from, string to)
        {
            IQueryable<Movie> movies = context.Movies
                .OrderBy(m => m.Id)
                .Include(m => m.Comments);

            DateTime fromDate = DateTime.Parse(from);
            DateTime toDate = DateTime.Parse(to);

            // LINQ
            //  var result = _context.Movies
            // nu mai este query de sql daca nu mai lucrez cu db(context)
            var result = this.GetMovies()       // nu lucrez cu context.Movies pt ca imi treb o lista de modele care includ nr comm pt tabelget Angular
                .Where(o => (o.DateAdded > fromDate) && (o.DateAdded < toDate));

            var query = result
                .OrderBy(o => o.YearOfRelease);

            return query;
        }









        public Movie GetMovieById(long id)
        {
            // or context.Movies.Find()
            return context.Movies
                .Include(m => m.Comments)
                .FirstOrDefault(m => m.Id == id);
        }








        public Movie Update(long id, Movie movie)
        {
            var existing = context.Movies.AsNoTracking().FirstOrDefault(f => f.Id == id);
            
            if (existing == null)
            {
                context.Movies.Add(movie);
                context.SaveChanges();
                return movie;
            }

            movie.Id = id;
            context.Movies.Update(movie);
            context.SaveChanges();
            return movie;
        }




    }
}
