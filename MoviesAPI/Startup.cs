using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Movies.Models;
using MoviesAPI.Helpers;
using MoviesAPI.Services;
using MoviesAPI.Models;
using Microsoft.AspNetCore.Identity;
using AutoMapper;


namespace MoviesAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // JWT Configuration
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });




            services
                .AddControllers()

                //.AddJsonOptions(options =>
                //{
                //    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());      // cand nu am NewtonSoft ca sa imi apara enum ca string si nu int
                //    options.JsonSerializerOptions.IgnoreNullValues = true;                            // cand nu am NewtonSoft pun adnotari la clasa model tip enum si import newtonsoft

                //})

                .AddNewtonsoftJson()

                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));


            //services.AddControllers().AddNewtonsoftJson(options =>
            //   options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddControllers().AddNewtonsoftJson(options =>
            {     // by default, Newtonsoft face camelCase si eu vreau PascalCase
                options.UseMemberCasing();                      // daca nu aveam Newtonsoft, by default era PascalCase
            });



            // Add EF services to the services container.
            services.AddDbContext<MoviesDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));  // DefaultConn e specificat in appstettings.json

            services.AddCors(); // Cross origins between Angular and API


            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());  // AutoMapper din Nuget


            // https://stackoverflow.com/questions/38138100/addtransient-addscoped-and-addsingleton-services-differences
            // Dependency Injection
            services.AddScoped<IMovieService, MovieService>();
            // configure DI (Dependency Injection) for application services
            services.AddScoped<IUserService, UserService>();




            //// Facebook authentication
            //services.AddAuthentication().AddFacebook(facebookOptions =>
            //{
            //    facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
            //    facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            //});




        }






        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseAuthentication();


            // Configurations to consume the Web API from Port 4200 (Angular App)
            app.UseCors(options =>
             options.WithOrigins("http://localhost:4200")        // Angular URL
                .AllowAnyMethod()
                .AllowAnyHeader());
            //    .WithExposedHeaders("X-Pagination"));   // pt Angular

            // https://dzone.com/articles/using-the-angular-material-paginator-with-aspnet-c


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }



            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });




        }
    }
}
