using AutoMapper;
using MoviesAPI.Models;
using MoviesAPI.ViewModels;

namespace MoviesAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, AuthenticatePostModel>();
            CreateMap<RegisterModel, User>();
         //   CreateMap<UpdateModel, User>();
        }
    }
}