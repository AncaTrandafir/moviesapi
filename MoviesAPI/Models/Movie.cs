using Movies.Models;
using MoviesAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Movies.Models
{

    public enum Genre
    {
        [EnumMember]        // using System.Runtime.Serialization
        Adventure,
        Comedy,
        Horror,
        SciFi
    }


    public class Movie
    {

        [Key]
        public long Id { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }

        [StringLength(150, MinimumLength = 3)]
        public string Description { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]        // using Newtonsoft Json Converters
        public Genre Genre { get; set; }

        public int Duration { get; set; }

        [Range(1900, 2020)]
        public int YearOfRelease { get; set; }

        [StringLength(30, MinimumLength = 3)]
        public string Director { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        //public DateTime DateAdded { get; set; }

        // mai usor fac dateAdded string sa fie compatibil cu dateAdded din Angular care la fel mai usor e string
        // avand in vedere ca in c# nu exista doar Date ci DateTime care memoreaza si ora, si deci ma incurca

        // public string DateAdded { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateAdded { get; set; }

        [Range(1, 10)]
        public int Rating { get; set; }

        public bool Watched { get; set; }
        [JsonIgnore]
        public List<Comment> Comments { get; set; }

        public User AddedBy { get; set; }
    }

}
