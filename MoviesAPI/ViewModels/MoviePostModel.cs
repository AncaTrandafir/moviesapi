﻿using Movies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.ViewModels
{
    public class MoviePostModel                     // excludem comments si addedBy cand facem Post, pt ca addedBy se va completa automat
                                                    // nu putem facem [JsonIgnore] la addedBy in Model pt ca imi treb addedBy la GetRequest
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Genre Genre { get; set; }
        public int Duration { get; set; }
        public int YearOfRelease { get; set; }
        public string Director { get; set; }
        public DateTime DateAdded { get; set; }
        public int Rating { get; set; }
        public bool Watched { get; set; }


        public static Movie PostMovieModel(MoviePostModel movie)            // metoda inversa fata de GetMovieMovel
        {                                                                   // primeste un model si returneaza un Movie
            return new Movie       // populeaza cu date din movie
            {
                Id = movie.Id,
                Title = movie.Title,
                Description = movie.Description,
                Genre = movie.Genre,
                Duration = movie.Duration,
                YearOfRelease = movie.YearOfRelease,
                Director = movie.Director,
                DateAdded = movie.DateAdded,
                Rating = movie.Rating,
                Watched = movie.Watched,

            };
        }
    }
}
